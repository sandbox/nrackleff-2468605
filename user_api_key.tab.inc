<?php
/**
 * @file
 * The display for User API Key tab.
 */

/**
 * Displays the API key on the user view.
 * Access to the tab is handled in the menu function.
 * 
 * @param object $account 
 *   The user object on which the operation is being performed.
 * @return string
 *   Returns the HTML to display the API key on the user view.
 */
function _user_api_key_display_key($account) {
  $content = array();
  $instance = field_info_instance('user', 'user_apikey', 'user');
  $desc = variable_get('user_api_key_message', '');
  $item = array(
    'title' => t('API key:'),
    'data' => @$account->user_apikey['und']['0']['value'],
    'message' => t('!desc', array('!desc' => $desc)),
  );
  if ($item['data']) {
    $content[] = array(
      '#type' => 'markup',
      '#markup' => theme('user_api_key_item', $item),
    );
  } 
  else {
    $content[] = array(
      '#type' => 'markup',
      '#markup' => t('API key not found.'),
    );
  }
  return $content;
}

/**
 * Sanitize the output before it goes to the template file.
 */
function user_api_key_preprocess_user_api_key_item(&$variables) {
  $variables['message'] = filter_xss_admin($variables['message']);
}
