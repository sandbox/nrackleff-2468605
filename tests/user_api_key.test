<?php
/**
 * @file
 * Tests for the hello world module.
 */

class UserApiKeyTests extends DrupalWebTestCase {

  /**
   * Metadata about our test case.
   */
  public static function getInfo() {
    return array(
      'name' => t('User API Key Functional'),
      'description' => t('Functional tests for the User API Key module.'),
      'group' => t('User API Key Group'),
    );
  }

  /**
   * Perform any setup tasks for our test case.
   */
  public function setUp() {
    // Enable this module and its dependencies.
    parent::setUp(array('field_sql_storage', 'field_extrawidgets', 'user_api_key'));

    // Create a user with 'use api key' permissions.
    $this->apikey_user = $this->drupalCreateUser(array('use API key', 'access user profiles'));

    // Create a user without use api key permissions.
    $this->non_apikey_user = $this->drupalCreateUser(array('access user profiles')); 

    // Create a user that is able to set the api key message.
    $this->apikey_admin = $this->drupalCreateUser(array('use API key', 'administer site configuration', 'access user profiles'));

  }

  /**
   * Perform tests related to viewing the API key.
   */
  function testKeyViewing() {
    // Verify that a user with 'use api key' permissions can see their key on the tab.
    $this->drupalLogin($this->apikey_user);
    $this->drupalGet('user/' . $this->apikey_user->uid . '/api-key');
    $this->assertText('API key:', 'User with permission can view their API key.');

    // Verify that the key is not visible on the edit profile page.
    $this->drupalGet('user/' . $this->apikey_user->uid . '/edit');
    $this->assertNoText('User API key', 'User API key is not found on edit form.');
    $this->drupalLogout();

    // Verify that a user without 'use api key' permissions does not see a key tab.
    $this->drupalLogin($this->non_apikey_user);
    $this->drupalGet('user/' . $this->non_apikey_user->uid . '/api-key');
    $this->assertText('Access denied', 'User without permission cannot view their API key.');
    $this->drupalLogout();

    // Verify that a user cannot see another users key.
    $this->drupalLogin($this->apikey_admin);
    $this->drupalGet('user/' . $this->apikey_user->uid . '/api-key');
    $this->assertText('Access denied', 'User my not view another user\'s API key.');
    $this->drupalLogout();
  }

  /**
   * Perform tests related to configuring the API key message.
   */
  function testKeyConfiguration() {
    // Verify that a user with 'administer site configuration' permissions can edit the API key message.
    $this->drupalLogin($this->apikey_admin);
    $this->drupalGet('admin/config/user-api-key/settings');
    $this->assertFieldByName('user_api_key_message', NULL, 'The message field is present.');
    $data = array(
      'user_api_key_message' => 'Awesome!'
    );
    $this->drupalPost('admin/config/user-api-key/settings', $data, 'Save configuration');
    $this->assertText('The configuration options have been saved.', 'User with permission was able to save message.');

    // Verify that the API key message shows up on the API key tab.
    $this->drupalGet('user/' . $this->apikey_admin->uid . '/api-key');
    $this->assertText('Awesome!', 'The API key message shows up on the API key tab.');
    $this->drupalLogout();
    
    // Verify that a user without 'administer site configuration' permissions cannot edit the api key message.
    $this->drupalLogin($this->apikey_user);
    $this->drupalGet('admin/config/user-api-key/settings');
    $this->assertText('Access denied', 'User without permission cannot edit the message.');
    $this->drupalLogout();
  }
} // End class UserApiKeyTests
