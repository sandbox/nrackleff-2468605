CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting


INTRODUCTION
------------
The User API Key module creates a read-only API key for each existing and new user. It adds a line to the permission screen where the site administrator can set which user roles may access their API keys. It includes settings configuration page /admin/config/user-api-key/settings where the site administrator can add a site-specific terms of use message to be displayed below the API Key on the user profile. 

Upon installation the module will create a read-only API key for each user in the database. It will add keys for new users as they are added. It will also run on cron to scan the database for any user that does not have a key and create one for such user if found.

The module adds an 'API key' tab to the user page. The tab is viewable only to a user with 'Use API key' permissions or to an administrator. It is not viewable to other users. 

This module does not provide authentication on its own. Your module would need to check the key against the user and permissions. This module came about as a result of a custom module that we built for the National Institutes of Health 3D Print API. http://3dprint.nih.gov/


REQUIREMENTS
------------
This module requires the following modules:
 * Field (core)
 * Field SQL storage (core)
 * Field Extra Widgets (https://www.drupal.org/project/field_extrawidgets) 


INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further information. 

When the module is disabled, the keys persist, but are no longer visible. Re-enabling the module will make the keys visible again.

When the module is uninstalled, all keys are deleted from the database. Reinstalling the module will generate new keys for all users. Do not uninstall the module unless you want to wipe out all keys!


CONFIGURATION
-------------
Configure user permissions in Administration » People » Permissions:

 * Use API key
   Users may utilize their API key. 

Configure the message to be displayed at the bottom of the API Key on the user profile by visiting /admin/config/user-api-key/settings.

The module removes the API key from the user profile edit screen by default. It is best leave this hidden so users can not edit it and accidentally create a non-unique key which might lead to security issues. 


TROUBLESHOOTING
---------------
If users are unable to view their keys, check the permissions to make sure the 'Use API key' permission is enabled for the desired user role(s).

There is no admin interface to regenerate an individual user token. If a token is compromised, it will need to be manually deleted from the database. It will be regenerated on the next run of cron. 

 * Run the MySQL command: 
   DELETE FROM field_data_user_apikey WHERE entity_id = [numeric user id];

 * Navigate to Administration » Reports » Status report and click the "run 
   cron manually" link to generate tokens for users that do not have them.


CONTACT
-------

Current Maintainers:
Nancy Rackleff
https://www.drupal.org/u/nrackleff

Greg Lund-Chaix
https://www.drupal.org/u/gchaix

This project has been sponsored by:
Squishymedia
http://squishymedia.com