<?php
/**
 * @file
 * Contains functions related to creating and displaying API keys for users.
 */

/** 
 * Implements hook_disable().
 */
function user_api_key_disable() {
  $instance = field_info_instance('user', 'user_apikey', 'user');
  // Set field to hidden in case it has been changed while in use
  $instance['widget']['type'] = 'field_extrawidgets_hidden';
  $instance['display']['default']['type'] = 'hidden';
  field_update_instance($instance);
}

/**
 * Implements hook_user_presave().
 *
 * Generates an API key for every new user that is created.
 */
function user_api_key_user_presave(&$edit, $account, $category) {
  // only do this if it is a new account
  if ($account->is_new) {
    try {
      $name = $edit['name'];
      // generate the token
      $new_token = _user_api_key_generate($name);
      // update the user - add the token
      $token = @$edit['user_apikey']['und'][0]['value'];
      if (!$token) {
        $edit['user_apikey']['und'][0]['value'] = $new_token;
      }
    } catch (Exception $e) {
      watchdog('User API key', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  } 
}

/**
 * Implements hook_theme().
 */
function user_api_key_theme() {
  return array(
    'user_api_key_item' => array(
      'arguments' => array('item' => NULL, 'data' => NULL, 'message' => NULL),
      'template' => 'user-api-key-item',
    ),
  );
}

/**
 * Implements hook_cron().
 *
 * Generate an API keys for any users that managed to get into the database without API keys.
 */
function user_api_key_cron() {
  _user_api_key_make_for_all();
}

/**
 * Implements hook_menu().
 */
function user_api_key_menu() {
  // Adds a tab to user page.
  $items['user/%user/api-key'] = array(
    'title' => 'API key',
    'page callback' => '_user_api_key_display_key',
    'page arguments' => array(1),
    'access callback' => '_user_api_key_viewable',
    'access arguments' => array(1),
    'file' => 'user_api_key.tab.inc',
    'type' => MENU_LOCAL_TASK,
  );
  // Adds item to admin config page.
  $items['admin/config/user-api-key'] = array(
    'title' => 'User API Key',
    'description' => 'Set the message to be displayed to the user along with the API key.',
    'position' => 'right',
    'weight' => '-5',
    'page callback' => 'system_admin_menu_block_page',
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  ); 
  $items['admin/config/user-api-key/settings'] = array(
    'title' => 'User API Key Settings',
    'description' => 'Set the message to be displayed to the user along with the API key.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_api_key_admin_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'file' => 'user_api_key.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  ); 
  return $items;
}

/**
 * Implements hook_permission().
 */
function user_api_key_permission() {
  return array(
      'use API key' => array(
        'title' => t('Use API key'),
        'description' => t('Users may utilize their API key.'),
        'restrict access' => TRUE,
      ),
  );
}

/**
 * Generates an API key.
 *
 * @param $name string 
 *   A valid user name. 
 * @return mixed
 *   Returns a token or FALSE if unable to create token. 
 */
function _user_api_key_generate($name) {
  // Generate the token based on username, timestamp and password salt.
  GLOBAL $drupal_hash_salt;
  if ($name) {
    $time_name = time() . $name;
    $hmac = hash_hmac('sha256', $time_name, $drupal_hash_salt);
    return $hmac;
  } 
  else {
    return FALSE;
  }
}

/**
 * Loops through all users and generates keys for all that do not already have them.
 * 
 * @return boolean
 *   True if able to run through even if no users needed tokens.
 */
function _user_api_key_make_for_all() { 
  $success = TRUE;
  $userids = array();
  $allusers = array();

  // Get all the users that DO have values
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'user')
    ->fieldCondition('user_apikey', 'value', 'NULL', '!=');
  $result = $query->execute();

  $result_count = count($result['user']);

  // If there were no users with keys, then get all users. Otherwise, just get the users without keys.
  if ($result_count == 0) {
    $allusers = entity_load('user');
  }
  else {
    // Now get all the other users, that aren't in the list just retrieved.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'user')
      ->entityCondition('entity_id', array_keys($result['user']), 'NOT IN');
    $result_two = $query->execute();
  }
  
  // If we have any users without API keys
  if (is_array(@$result_two['user'])) {
    $userids = array_keys($result_two['user']);
    $allusers = entity_load('user', $userids);
  }

  // Loop through and edit each user, assigning them a unique API key.
  foreach ($allusers as $thisuser) {
    // Assign the new API key to all but the anonymous user & those that already have tokens.
    if (($thisuser->uid != 0) && !$thisuser->user_apikey) {
      watchdog('User API key', "Assigning an API key to " . $thisuser->name , array(), WATCHDOG_NOTICE);
      try {
        $data = $thisuser->name;
        $userkey = _user_api_key_generate($data);
        if ($userkey) {
          $thisuser->user_apikey['und'][0]['value'] = $userkey;
          // Save the user object
          user_save($thisuser);
        }
      } 
      catch (Exception $e) {
        watchdog('User API key', $e->getMessage(), array(), WATCHDOG_ERROR);
        $success = FALSE;
      } 
    } // End if
  } // End for each 
  return $success;
}

/**
 * Check if the viewer is the owner of this profile & if the viewer has permission to use tokens, or if user is administrator.
 *
 * @return boolean
 *   True if user has permission to use token.
 */
function _user_api_key_viewable($account) {
  global $user;
  if (($user->uid == $account->uid) && user_access('use API key')) {  
    return TRUE;
  } 
  elseif (in_array('administrator', array_values($user->roles))) {
    return TRUE;
  } 
  return FALSE;
}

/**
 * Implements template_preprocess_user_profile
 * 
 * Do not show the API key on the user profile view page.
 */
function user_api_key_preprocess_user_profile(&$vars) {
  unset($vars['user_profile']['user_apikey']);
}
