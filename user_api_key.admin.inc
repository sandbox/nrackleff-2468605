<?php 
/**
 * @file
 * The settings screen for User API Key.
 */

/**
 * Allows for custom message to be displayed to the user along with the API key.
 */
function user_api_key_admin_form($form, &$form_state) {
  $form['user_api_key_message'] = array(
    '#type' => 'textarea',
    '#title' => t('User API Key Message'),
    '#default_value' => variable_get('user_api_key_message', ''),
    '#description' => t('Descriptive text to display to the user along with the API key.'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
